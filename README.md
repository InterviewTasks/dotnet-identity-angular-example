# This is a interview task
The job was done using ASP.NET Core Web API 2, Angular 4.2.5, MySql, Entity Framework, ASP.NET Identity, Microsoft Visual Studio 2017

# Installation

##		Running the ASP.NET Core Web API and Running the Angular 4 Client

1. Download the project code from the GitLab link above.
2. Restore the database from a file database\Dump.anc1.sql.
3. Modify connect string (the file Web\appsettings.json).
4. To develop and run Angular 4 applications locally, download and install [NodeJS] (https://nodejs.org/) - javascript runtime environment that includes the node package manager (npm).
5. Open the project root folder in Visual Studio 2017.
6. Waiting for the download of NuGet, SDK, NPM packages.

##   	Run project 

Start the application by pressing F5 or by selecting Debug -> Start Debugging from the top menu in VS, this runs the web api at http://localhost:54986 , a browser window should automatically open to the application at http://localhost:54986




# Screenshots

###		Home

![alt Home](screenshot/1-home.png)



###		Create user (input)
![alt Create user (input)](screenshot/2-create-input.png)


###		Create user (exists user)
![alt Create user (exists user)](screenshot/3-create-exists-user.png)


###		Create user (success)
![alt Create user (success)](screenshot/4-create-success.png)


###		Users list
![alt Users list](screenshot/5-userlist.png)


###		Users list (detail)
![alt Users list (detail)](screenshot/6-userlist-detail.png)


###		Users list (filter)
![alt Users list (filter)](screenshot/7-userlist-filter.png)
