﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Web;
using Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    public class ApplicationUsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ApplicationUsersController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole> roleManager
            )
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: api/user
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<ApplicationUserShortItem> UsersList()
        {
            return from au in _context.ApplicationUsers
                   orderby au.FullName
                   select new ApplicationUserShortItem
                     {
                        id = au.Id,
                        email = au.Email,
                        fullname = au.FullName,
                        phonenumber = au.PhoneNumber
                     };
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> UserCreate([FromBody]ApplicationUserShortItem postUser)
        {
            Int32 selectedUsers = (from user in _context.ApplicationUsers
                                where user.FullName == postUser.fullname
                                select user).Count()
                                ;
            // Создание пользователя, если он не существует
            if (await _userManager.FindByEmailAsync(postUser.email) != null)
                return StatusCode(StatusCodes.Status204NoContent); // если почта существует (это условие IdentityUser)
            else if (selectedUsers == 0)
            {
                ApplicationUser user = new ApplicationUser
                {
                    FullName = postUser.fullname,
                    Email = postUser.email,
                    UserName = postUser.email,
                    PhoneNumber = postUser.phonenumber,
                    LockoutEnabled = false
                };

                //  Предупреждение: не проверять учетные данные любого типа в системы управления
                var result = await _userManager.CreateAsync(user, password: postUser.password);

                if (!result.Succeeded) // Возвращает 500 если завершилось неуспешно
                    return StatusCode(StatusCodes.Status500InternalServerError);

                // Возврат 201
                return StatusCode(StatusCodes.Status201Created);
            }
            else
            {
                // Возврат 200, как выполнение, но не создано, т.к. пользовать уже существует
                return StatusCode(StatusCodes.Status200OK);
            }
        }
    }
}