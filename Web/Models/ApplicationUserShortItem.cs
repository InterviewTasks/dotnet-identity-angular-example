using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Web.Models
{
    public class ApplicationUserShortItem
    {
        public String id { get; set; }
        public String email { get; set; }
        public String fullname { get; set; }
        public String phonenumber { get; set; }
        public String password { get; set; }
    }
}
