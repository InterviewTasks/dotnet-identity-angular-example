﻿import { Component, Inject, Pipe, PipeTransform, Injectable, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { User } from '../_models/index';
import { UserService } from '../_services/index';

@Pipe({
    name: 'searchfilter'
})
@Injectable()
export class SearchFilterPipe implements PipeTransform {
    transform(items: User[], value: string): any[] {
        if (!items || !value) {
            return items;
        }
        return items.filter(e => e.fullname.toLowerCase().includes(value.toLocaleLowerCase()));
    }
}

@Component({
    selector: 'userlist-form',
    templateUrl: './userlist.component.html'
})

export class UserListComponent {

    public form: FormGroup;
    public users: User[];

    constructor(
        private modal: Modal,
        private service: UserService
    ) {
        this.loadAllUsers();
        this.form = new FormGroup({ searchstring: new FormControl() });
    }

    private loadAllUsers() {
        this.service.getAll().subscribe(users => { this.users = users.json() as User[]},
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    clearsearchstring() {
        this.form.setValue({ searchstring: '' });
    }

    onGetInfo(id: string) {

        function getIndexById(_id: string, _users: User[]): number {
            for (var idx = 0, l = _users.length; _users[idx] && _users[idx].id !== _id; idx++);
            return idx === l ? -1 : idx;
        };

        let SelectedIndex: number = getIndexById(id, this.users);
        let MessageInfo: string =
            '<b>Пользователь: ' + this.users[SelectedIndex].fullname + '</b>' +
            '<ul>' +
            '<li>Телефон: ' + this.users[SelectedIndex].phonenumber + '</li>' +
            '<li>Почта: ' + this.users[SelectedIndex].email + '</li>' +
            '</ul>';
        const dialogRef = this.modal.alert()
            .size('lg')
            .showClose(true)
            .title('Подробно')
            .body(MessageInfo)
            .open();
        //dialogRef
        //    .then(dialogRef => {
        //        dialogRef.result.then(result => alert('Спасибо за просмотр'));
        //    });
    }
}