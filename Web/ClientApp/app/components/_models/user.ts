﻿export class User {
    id: string;
    fullname: string;
    password: string;
    email: string;
    phonenumber: string;
}