﻿import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { AlertService, UserService } from '../_services/index';
import { User } from '../_models/index';

@Component({
    selector: 'createuser',
    templateUrl: './createuser.component.html'
})

export class CreateUserComponent {
    model: User = new User();
    loading = false;

    constructor(
        private router: Router,
        private modal: Modal,
        private service: UserService,
        private alertService: AlertService
    ) { }

    register() {
        this.service.create(this.model)
            .subscribe(
            data => {
                switch (data.status) {
                    case 201: {
                        const dialogRef = this.modal.alert()
                            .size('lg')
                            .showClose(true)
                            .title('Успешно')
                            .body('Пользователь: <b><' + this.model.fullname + '></b> создан')
                            .open();
                        this.router.navigate(['/userlist']);
                        break;
                    }
                    case 200: {
                        const dialogRef = this.modal.alert()
                            .size('lg')
                            .showClose(true)
                            .title('Ошибка')
                            .body('Пользователь: <b><' + this.model.fullname + '></b> уже существует')
                            .open();
                        break;
                    }
                    case 204: {
                        const dialogRef = this.modal.alert()
                            .size('lg')
                            .showClose(true)
                            .title('Ошибка')
                            .body('Почтовый адрес: <b><' + this.model.email + '></b> уже используется')
                            .open();
                        break;
                    }
                    default: {
                        const dialogRef = this.modal.alert()
                            .size('lg')
                            .showClose(true)
                            .title('Ошибка')
                            .body('Ошибка: <b><' + data.status.toString() + '></b>')
                            .open();
                        break;
                    }
                }
            },
            error => {
                const dialogRef = this.modal.alert()
                    .size('lg')
                    .showClose(true)
                    .title('Ошибка')
                    .body('Ошибка создания пользователя: <b><' + this.model.fullname + '></b><br>' + error.json())
                    .open();
            }
            );
    }
}