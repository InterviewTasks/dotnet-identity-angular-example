﻿import { Inject, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(
        private httpClient: Http,
        @Inject('BASE_URL') public baseUrl: string
    ) { }

    public getAll(): Observable<Response> {
        return this.httpClient.get(this.baseUrl + 'api/user');
    }

    public create(user: User): Observable<Response> {
        let result: Observable<Response> = this.httpClient.post(this.baseUrl + 'api/user', user);
        return result;
    }
}

