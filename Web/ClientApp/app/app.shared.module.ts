import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { CreateUserComponent } from './components/createuser/createuser.component';
import { UserListComponent } from './components/userlist/userlist.component';
import { SearchFilterPipe } from './components/userlist/userlist.component';
import { AlertService } from './components/_services/index';
import { UserService } from './components/_services/index';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        CreateUserComponent,
        UserListComponent,
        SearchFilterPipe
    ],
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'createuser', component: CreateUserComponent },
            { path: 'userlist', component: UserListComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
        AlertService,
        UserService
   ]
})
export class AppModuleShared {
}
